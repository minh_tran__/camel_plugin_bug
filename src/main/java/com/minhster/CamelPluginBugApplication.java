package com.minhster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelPluginBugApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelPluginBugApplication.class, args);
	}

}
