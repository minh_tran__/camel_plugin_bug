package com.minhster;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MyType {
  private int foo;
}
