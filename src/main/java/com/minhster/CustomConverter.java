package com.minhster;

import org.apache.camel.Converter;

@Converter(generateBulkLoader = true)
public class CustomConverter {

  @Converter
  public static MyType convert(Integer num) {
    return MyType.builder().foo(num).build();
  }
}
