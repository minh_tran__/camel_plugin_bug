package com.minhster;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class Router extends RouteBuilder {

  @Override
  public void configure() throws Exception {
    from("direct:start")
      .convertBodyTo(MyType.class);
  }

}
