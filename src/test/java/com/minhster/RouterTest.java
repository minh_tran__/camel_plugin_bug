package com.minhster;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@CamelSpringBootTest
@SpringBootTest
class RouterTest {

  @Autowired
  private ProducerTemplate producer;

  @Test
  void test() {
    assertThat(producer.requestBody("direct:start", 1, MyType.class).getFoo()).isEqualTo(1);
  }

}
